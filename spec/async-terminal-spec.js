import { AsyncTerminal } from '../index';

describe('AsyncTerminal', function() {

  beforeEach(function() {
    this.terminalContainer = document.createElement('div');
    this.rowContainer = document.createElement('div');
    this.terminal = new AsyncTerminal(this.terminalContainer, this.rowContainer);
  });

  describe('constructor', function() {

    it('should initialize the queue', function() {
      expect(this.terminal.writeQueue).toEqual([]);
      expect(this.terminal.timeoutID).toBeNull();
    });

  });

  describe('write', function() {

    it('should render text asynchronously', function(finish) {
      this.terminal.write('test', { done: () => {
        expect(this.rowContainer.innerHTML).toBe(
          '<div><span>test</span></div>'
        );

        finish();
      }});
    });

    it('should not render null data', function(finish) {
      this.terminal.write(null, { done: () => {
        expect(this.rowContainer.innerHTML).toBe('');

        finish();
      }});
    });

    it('should not require callbacks', function(finish) {
      this.terminal.write('test');

      setTimeout(() => {
        expect(this.rowContainer.innerHTML).toBe(
          '<div><span>test</span></div>'
        );

        finish();
      }, 0);
    });

    it('should queue up write requests', function(finish) {
      let first, second, third;

      this.terminal.write('test', { done: () => {
        first = this.rowContainer.innerHTML;
      }});

      this.terminal.write('\nmore', { done: () => {
        second = this.rowContainer.innerHTML;
      }});

      this.terminal.write(' writes', { done: () => {
        third = this.rowContainer.innerHTML;

        expect(first).toBe(
          '<div><span>test</span></div>'
        );

        expect(second).toBe(
          '<div><span>test</span></div>' +
          '<div><span>more</span></div>'
        );

        expect(third).toBe(
          '<div><span>test</span></div>' +
          '<div><span>more&nbsp;writes</span></div>'
        );

        finish();
      }});
    });

    it('should call the tick callback after each chunk is written', function(finish) {
      const innerHTML = [];
      const tick = jasmine.createSpy('tick').and.callFake(() => {
        innerHTML.push(this.rowContainer.innerHTML);
      });

      this.terminal.write('test\ntick', { chunkSize: 1, tick, done: () => {
        expect(tick.calls.allArgs()).toEqual([[1], [1], [1], [1], [1], [2], [2], [2], [2]]);
        expect(innerHTML).toEqual([
          '<div><span>t</span></div>',
          '<div><span>te</span></div>',
          '<div><span>tes</span></div>',
          '<div><span>test</span></div>',
          '<div><span>test</span></div>',
          '<div><span>test</span></div><div><span>t</span></div>',
          '<div><span>test</span></div><div><span>ti</span></div>',
          '<div><span>test</span></div><div><span>tic</span></div>',
          '<div><span>test</span></div><div><span>tick</span></div>',
        ]);

        finish();
      }});
    });

  });

  describe('reset', function() {

    it('should reset the queue', function(finish) {
      const done = jasmine.createSpy('done');
      this.terminal.write('test', { done });
      this.terminal.reset();
      this.terminal.write('reset', { done: () => {
        expect(done).not.toHaveBeenCalled();
        expect(this.rowContainer.innerHTML).toBe(
          '<div><span>reset</span></div>'
        );

        finish();
      }});
    });

    it('should reset the queue in the middle of a write', function(finish) {
      const done = jasmine.createSpy('done');
      this.terminal.write('test', { chunkSize: 1, done, tick: () => {
        this.terminal.reset();
        this.terminal.write('reset', { done: () => {
          expect(done).not.toHaveBeenCalled();
          expect(this.rowContainer.innerHTML).toBe(
            '<div><span>reset</span></div>'
          );

          finish();
        }});
      }});
    });

  });

});
