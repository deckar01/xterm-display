import { Character } from '../index';
import { Attribute } from '../index';

describe('Character', function() {

  describe('constructor', function() {

    it('should initialize the default properties', function() {
      const character = new Character();

      expect(character.value).toBe('\u00a0');
      expect(character.attributes).toEqual({});
    });

    it('should have the passed properties', function() {
      const character = new Character('t', {bold: true});

      expect(character.value).toBe('t');
      expect(character.attributes).toEqual({bold: true});
    });

    it('should change non-printable characters to spaces', function() {
      const character = new Character('\x04');

      expect(character.value).toBe('\u00a0');
    });

    it('should encode spaces as nonbreaking whitespace', function() {
      const character = new Character(' ');

      expect(character.value).toBe('\u00a0');
    });

    it('should encode amperstands as unicode amperstands', function() {
      const character = new Character('&');

      expect(character.value).toBe('\u0026');
    });

    it('should encode less than signs as unicode', function() {
      const character = new Character('<');

      expect(character.value).toBe('\u003c');
    });

    it('should encode greater than signs as unicode', function() {
      const character = new Character('>');

      expect(character.value).toBe('\u003e');
    });

  });

  describe('customization', function() {

    beforeEach(function() {
      this.originalEncoding = {};
      Object.keys(Character.encoding).forEach((key) => {
        this.originalEncoding[key] = Character.encoding[key];
      });
    });

    afterEach(function() {
      Character.encoding = this.originalEncoding;
    });

    it('should encode custom characters', function() {
      Character.encoding['\x07'] = '\u2407';

      const character = new Character('\x07');

      expect(character.value).toBe('\u2407');
    });

    it('should not encode spaces if the encoding is removed', function() {
      delete Character.encoding[' '];

      const character = new Character('\x02');

      expect(character.value).toBe(' ');
    });

  });

});
