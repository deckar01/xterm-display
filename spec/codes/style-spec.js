import { Terminal } from '../../index';

describe('style codes', function() {

  beforeEach(function() {
    this.terminalContainer = document.createElement('div');
    this.rowContainer = document.createElement('div');
    this.terminal = new Terminal(this.terminalContainer, this.rowContainer);
  });

  describe('colors', function() {

    it('should reset text styles', function() {
      this.terminal.write('\x1b[1mtest \x1b[0mreset');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span class="xterm-bold">test&nbsp;</span><span>reset</span></div>'
      );
    });

    it('should render colored text', function() {
      this.terminal.write('\x1b[34mtest');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span class="xterm-color-4">test</span></div>'
      );
    });

    it('should render colored backgrounds', function() {
      this.terminal.write('\x1b[42mtest');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span class="xterm-bg-color-2">test</span></div>'
      );
    });

    it('should render colored text with extended colors', function() {
      this.terminal.write('\x1b[38;5;255mtest');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span class="xterm-color-255">test</span></div>'
      );
    });

    it('should render colored backgrounds with extended colors', function() {
      this.terminal.write('\x1b[48;5;127mtest');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span class="xterm-bg-color-127">test</span></div>'
      );
    });

    it('should reset the text color', function() {
      this.terminal.write('\x1b[34;1mcolor\x1b[39m test');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span class="xterm-color-4 xterm-bold">color</span><span class="xterm-bold">&nbsp;test</span></div>'
      );
    });

    it('should reset the background color', function() {
      this.terminal.write('\x1b[42;3mcolor\x1b[49m test');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span class="xterm-bg-color-2 xterm-italic">color</span><span class="xterm-italic">&nbsp;test</span></div>'
      );
    });

    it('should ignore unsupported color extensions', function() {
      this.terminal.write('\x1b[38;42;1mtest');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span class="xterm-bold">test</span></div>'
      );
    });

    it('should ignore unsupported background extensions', function() {
      this.terminal.write('\x1b[48;42;3mtest');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span class="xterm-italic">test</span></div>'
      );
    });

  });

  describe('styles', function() {

    it('should render bold text', function() {
      this.terminal.write('\x1b[1mtest');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span class="xterm-bold">test</span></div>'
      );
    });

    it('should render italic text', function() {
      this.terminal.write('\x1b[3mtest');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span class="xterm-italic">test</span></div>'
      );
    });

    it('should render underline text', function() {
      this.terminal.write('\x1b[4mtest');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span class="xterm-underline">test</span></div>'
      );
    });

    it('should render conceal text', function() {
      this.terminal.write('\x1b[8mtest');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span class="xterm-conceal">test</span></div>'
      );
    });

    it('should render cross text', function() {
      this.terminal.write('\x1b[9mtest');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span class="xterm-cross">test</span></div>'
      );
    });

    it('should ignore unsupported style codes', function() {
      this.terminal.write('\x1b[1337mtest');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test</span></div>'
      );
    });

  });

});
