import { Terminal } from '../../index';

describe('escape codes', function() {

  beforeEach(function() {
    this.terminalContainer = document.createElement('div');
    this.rowContainer = document.createElement('div');
    this.terminal = new Terminal(this.terminalContainer, this.rowContainer);
  });

  describe('newline', function() {

    it('should escape control sequences', function() {
      this.terminal.write('\x1b[1mtest');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span class="xterm-bold">test</span></div>'
      );
    });

    it('should ignore unsupported control sequences', function() {
      this.terminal.write('\x1b[?test');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test</span></div>'
      );
    });

    it('should ignore unsupported escape codes', function() {
      this.terminal.write('\x1b?test');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test</span></div>'
      );
    });

  });

});
