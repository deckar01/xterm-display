import { Terminal } from '../../index';

describe('erase codes', function() {

  beforeEach(function() {
    this.terminalContainer = document.createElement('div');
    this.rowContainer = document.createElement('div');
    this.terminal = new Terminal(this.terminalContainer, this.rowContainer);
  });

  describe('erase in display', function() {

    it('should erase to the end of the display by default', function() {
      this.terminal.write('test\nerase\nto\nend\x1b[2;3H\x1b[J');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test</span></div>' +
        '<div><span>er</span></div>' +
        '<div></div>' +
        '<div></div>'
      );
    });

    it('should erase to the end of the display', function() {
      this.terminal.write('test\nerase\nto\nend\x1b[2;3H\x1b[0J');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test</span></div>' +
        '<div><span>er</span></div>' +
        '<div></div>' +
        '<div></div>'
      );
    });

    it('should erase to the beginning of the display', function() {
      this.terminal.write('test\nerase\nto\nend\x1b[2;3H\x1b[1J');

      expect(this.rowContainer.innerHTML).toBe(
        '<div></div>' +
        '<div><span>&nbsp;&nbsp;&nbsp;se</span></div>' +
        '<div><span>to</span></div>' +
        '<div><span>end</span></div>'
      );
    });

    it('should erase the entire display', function() {
      this.terminal.write('test\nerase\nto\nend\x1b[2J');

      expect(this.rowContainer.innerHTML).toBe(
        '<div></div>' +
        '<div></div>' +
        '<div></div>' +
        '<div></div>'
      );
    });

    it('should allow erasing an empty display', function() {
      this.terminal.write('\x1b[0J\x1b[1J\x1b[2J');

      expect(this.rowContainer.innerHTML).toBe('');
    });

  });

  describe('erase in line', function() {

    it('should erase to the end of the line by default', function() {
      this.terminal.write('test\nerase\nto\nend\x1b[2;3H\x1b[K');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test</span></div>' +
        '<div><span>er</span></div>' +
        '<div><span>to</span></div>' +
        '<div><span>end</span></div>'
      );
    });

    it('should erase to the end of the line', function() {
      this.terminal.write('test\nerase\nto\nend\x1b[2;3H\x1b[0K');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test</span></div>' +
        '<div><span>er</span></div>' +
        '<div><span>to</span></div>' +
        '<div><span>end</span></div>'
      );
    });

    it('should erase to the beginning of the line', function() {
      this.terminal.write('test\nerase\nto\nend\x1b[2;3H\x1b[1K');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test</span></div>' +
        '<div><span>&nbsp;&nbsp;&nbsp;se</span></div>' +
        '<div><span>to</span></div>' +
        '<div><span>end</span></div>'
      );
    });

    it('should erase the entire line', function() {
      this.terminal.write('test\nerase\nto\nend\x1b[2;3H\x1b[2K');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test</span></div>' +
        '<div></div>' +
        '<div><span>to</span></div>' +
        '<div><span>end</span></div>'
      );
    });

    it('should allow erasing an empty line', function() {
      this.terminal.write('\x1b[0K\x1b[1K\x1b[2K');

      expect(this.rowContainer.innerHTML).toBe('');
    });

  });

});
