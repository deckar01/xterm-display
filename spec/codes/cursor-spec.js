import { Terminal } from '../../index';

describe('cursor codes', function() {

  beforeEach(function() {
    this.terminalContainer = document.createElement('div');
    this.rowContainer = document.createElement('div');
    this.terminal = new Terminal(this.terminalContainer, this.rowContainer);
  });

  describe('new line', function() {

    it('should add a new line (line feed)', function() {
      this.terminal.write('test\nnewline');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test</span></div>' +
        '<div><span>newline</span></div>'
      );
    });

    it('should add a new line (vertical tab)', function() {
      this.terminal.write('test\x0bnewline');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test</span></div>' +
        '<div><span>newline</span></div>'
      );
    });

    it('should add a new line (form feed)', function() {
      this.terminal.write('test\x0cnewline');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test</span></div>' +
        '<div><span>newline</span></div>'
      );
    });

  });

  describe('move cursor', function() {

    it('should move the cursor to the beginning of the line', function() {
      this.terminal.write('test\rcarriage return');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>carriage&nbsp;return</span></div>'
      );
    });

    it('should move the cursor back', function() {
      this.terminal.write('tesr\x08t');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test</span></div>'
      );
    });

    it('should stop at the beginning when moving the cursor back too far', function() {
      this.terminal.write('r\x08\x08test');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test</span></div>'
      );
    });

    it('should move the cursor up', function() {
      this.terminal.write('test\nmoving\x1b[Aup');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test&nbsp;&nbsp;up</span></div>' +
        '<div><span>moving</span></div>'
      );
    });

    it('should move the cursor up n lines', function() {
      this.terminal.write('test\nmoving\nup\nmultiple\x1b[3Alines');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test&nbsp;&nbsp;&nbsp;&nbsp;lines</span></div>' +
        '<div><span>moving</span></div>' +
        '<div><span>up</span></div>' +
        '<div><span>multiple</span></div>'
      );
    });

    it('should stop at the top when moving the cursor up too far', function() {
      this.terminal.write('test\nmoving\x1b[3Aup');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test&nbsp;&nbsp;up</span></div>' +
        '<div><span>moving</span></div>'
      );
    });

    it('should move the cursor down', function() {
      this.terminal.write('test\nmoving\x1b[Bdown');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test</span></div>' +
        '<div><span>moving</span></div>' +
        '<div><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;down</span></div>'
      );
    });

    it('should move the cursor down n lines', function() {
      this.terminal.write('test\x1b[3Bdown');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test</span></div>' +
        '<div></div>' +
        '<div></div>' +
        '<div><span>&nbsp;&nbsp;&nbsp;&nbsp;down</span></div>'
      );
    });

    it('should move the cursor right', function() {
      this.terminal.write('test\x1b[Cright');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test&nbsp;right</span></div>'
      );
    });

    it('should move the cursor right n places', function() {
      this.terminal.write('test\x1b[3Cright');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test&nbsp;&nbsp;&nbsp;right</span></div>'
      );
    });

    it('should move the cursor left', function() {
      this.terminal.write('tesr\x1b[Dt');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test</span></div>'
      );
    });

    it('should move the cursor left n places', function() {
      this.terminal.write('trst\x1b[3De');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test</span></div>'
      );
    });

    it('should stop at the beginning when moving the cursor left too far', function() {
      this.terminal.write('re\x1b[3Dtest');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test</span></div>'
      );
    });

    it('should set the cursor to the beginning of the next line', function() {
      this.terminal.write('test\nsetting\x1b[Ethe row down');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test</span></div>' +
        '<div><span>setting</span></div>' +
        '<div><span>the&nbsp;row&nbsp;down</span></div>'
      );
    });

    it('should set the cursor to the beginning of the row n lines down', function() {
      this.terminal.write('test\nsetting the row\x1b[2Edown');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test</span></div>' +
        '<div><span>setting&nbsp;the&nbsp;row</span></div>' +
        '<div></div>' +
        '<div><span>down</span></div>'
      );
    });

    it('should set the cursor to the beginning of the previous line', function() {
      this.terminal.write('test\nsetting\x1b[Fthe row back');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>the&nbsp;row&nbsp;back</span></div>' +
        '<div><span>setting</span></div>'
      );
    });

    it('should set the cursor to the beginning of the row n lines up', function() {
      this.terminal.write('test\nsetting\nthe\nrow\x1b[3Fback');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>back</span></div>' +
        '<div><span>setting</span></div>' +
        '<div><span>the</span></div>' +
        '<div><span>row</span></div>'
      );
    });

    it('should stop at the top left when moving up too far', function() {
      this.terminal.write('test\nsetting\nthe\nrow\x1b[5Fback');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>back</span></div>' +
        '<div><span>setting</span></div>' +
        '<div><span>the</span></div>' +
        '<div><span>row</span></div>'
      );
    });

    it('should set the cursor to the beginning of the line', function() {
      this.terminal.write('rest\x1b[Gt');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test</span></div>'
      );
    });

    it('should set the cursor to the nth column', function() {
      this.terminal.write('\x1b[4Gtest');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>&nbsp;&nbsp;&nbsp;test</span></div>'
      );
    });

    it('should set the cursor to the top left (H)', function() {
      this.terminal.write('rest\nabsolute\x1b[Ht');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test</span></div>' +
        '<div><span>absolute</span></div>'
      );
    });

    it('should set the cursor to the top left (f)', function() {
      this.terminal.write('rest\nabsolute\x1b[ft');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test</span></div>' +
        '<div><span>absolute</span></div>'
      );
    });

    it('should set the absolute cursor position (H)', function() {
      this.terminal.write('trst\nabsolute\x1b[1;2He');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test</span></div>' +
        '<div><span>absolute</span></div>'
      );
    });

    it('should set the absolute cursor position (f)', function() {
      this.terminal.write('trst\nabsolute\x1b[1;2fe');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test</span></div>' +
        '<div><span>absolute</span></div>'
      );
    });

  });

});
