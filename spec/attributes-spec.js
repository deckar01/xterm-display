import { Attributes } from '../index';

describe('Attributes', function() {

  describe('default', function() {

    it('should default to empty attributes', function() {
      expect(Attributes.default()).toEqual({});
    });

  });

  describe('copy', function() {

    it('should copy attributes', function() {
      const attributes = {bold: true};

      const attributesCopy = Attributes.copy(attributes);

      expect(attributesCopy).not.toBe(attributes);
      expect(attributesCopy).toEqual(attributes);
    });

  });

  describe('equal', function() {

    it('should be true when attributes are identical', function() {
      const attributes1 = {bold: true, italic: true};
      const attributes2 = {bold: true, italic: true};

      const equality = Attributes.equal(attributes1, attributes2);

      expect(equality).toBe(true);
    });

    it('should be false when attributes are different', function() {
      const attributes1 = {bold: true};
      const attributes2 = {italic: true};

      const equality = Attributes.equal(attributes1, attributes2);

      expect(equality).toBe(false);
    });

  });

});
