import { Terminal } from '../index';
import { DefaultHandler } from '../index';

describe('Terminal', function() {

  beforeEach(function() {
    this.terminalContainer = document.createElement('div');
    this.rowContainer = document.createElement('div');
    this.terminal = new Terminal(this.terminalContainer, this.rowContainer);
  });

  describe('constructor', function() {

    it('should initialize the default properties', function() {
      expect(this.terminal.terminalContainer).toBe(this.terminalContainer);
      expect(this.terminal.rowContainer).toBe(this.rowContainer);
      expect(this.terminal.rowElements).toEqual([]);
      expect(this.terminal.characterMatrix).toEqual([]);
      expect(this.terminal.staleRows).toEqual({});
      expect(this.terminal.cursor).toEqual({x: 0, y: 0});
      expect(this.terminal.currentAttributes).toEqual({});
      expect(this.terminal.characterHandler).toBe(DefaultHandler);
    });

  });

  describe('write', function() {

    it('should render plain text', function() {
      this.terminal.write('test');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>test</span></div>'
      );
    });

    it('should not render null data', function() {
      this.terminal.write(null);

      expect(this.rowContainer.innerHTML).toBe('');
    });

  });

  describe('reset', function() {

    it('should reset the terminal', function() {
      this.terminal.write('\x1b[34mtest\n\x1b');
      this.terminal.reset();
      this.terminal.write('reset');

      expect(this.rowContainer.innerHTML).toBe(
        '<div><span>reset</span></div>'
      );
    });

  });

});
