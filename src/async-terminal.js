import { Terminal } from './terminal';

export class AsyncTerminal extends Terminal {

  /**
   * Reset a Terminal object to its initial state
   */
  reset() {
    super.reset();
    clearTimeout(this.timeoutID);
    this.timeoutID = null;
    this.writeQueue = [];
  }

  /**
   * A callback that runs after data is written to the terminal
   * @callback Terminal~writeCallback
   * @param {Number} rowCount - The total number of rows in the terminal
   */

  /**
   * Write ansi data to the terminal in chunks
   *
   * @param {String} [data=''] - String of ansi encoded data
   * @param {Object} [options={}] - Asynchronous options
   * @param {Number} [chunkSize=256] - The number of consecutive characters to parse at a time
   * @param {Number} [delay=0] - The amount of time to wait between
   * @param {Terminal~writeCallback} [tick] - Called after each chunk is written
   * @param {Terminal~writeCallback} [done] - Called after all chunks are written
   */
  write(data, { chunkSize = 256, delay = 0, tick = () => {}, done = () => {} } = {}) {
    // Add the write request to the queue
    this.writeQueue.push({ data: data || '', options: { chunkSize, delay, tick, done } });

    // Start processing the queue if it was empty
    if (this.writeQueue.length === 1) {
      this.writeChunk();
    }
  }

  writeChunk(chunkStart = 0) {
    const { data, options: { chunkSize, delay, tick, done } } = this.writeQueue[0];

    if (chunkStart < data.length) {
      // Write a chunk of data
      const chunkEnd = chunkStart + chunkSize;
      const chunk = data.slice(chunkStart, chunkEnd);
      super.write(chunk);

      // Schedule the next chunk to be written
      this.timeoutID = setTimeout(() => { this.writeChunk(chunkEnd); }, delay);
      tick(this.rowElements.length);
    } else {
      // Remove the completed arguments from the queue
      this.writeQueue.shift();
      done(this.rowElements.length);

      if (this.writeQueue.length > 0) {
        // Continue processing the next write request
        this.timeoutID = setTimeout(() => { this.writeChunk(); }, delay);
      } else {
        // Queue is empty
        this.timeoutID = null;
      }
    }
  }

}
