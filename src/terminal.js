import { Attributes } from './attributes';

import { DefaultHandler } from './handlers/default-handler';

export class Terminal {

  /**
   * Initialize a Terminal object
   *
   * @param {Element} terminalContainer
   * @param {Element} rowContainer
   */
  constructor(terminalContainer, rowContainer) {
    this.terminalContainer = terminalContainer;
    this.rowContainer = rowContainer;
    this.reset();
  }

  /**
   * Reset a Terminal object to its initial state
   */
  reset() {
    while (this.rowContainer.hasChildNodes()) {
      this.rowContainer.removeChild(this.rowContainer.lastChild);
    }
    this.rowElements = [];
    this.characterMatrix = [];
    this.staleRows = {};
    this.cursor = { x: 0, y: 0 };
    this.currentAttributes = Attributes.default();
    this.resetCharacterHandler();
  }

  /**
   * Write ansi data to the terminal
   *
   * @param {String} data - String of ansi encoded data
   */
  write(data) {
    const characters = data || '';
    characters.split('').forEach((character) => {
      const handler = this.characterHandler.getHandler(character);
      handler.call(this, character);
    });
    this.refresh();
  }

  markRowStale(y) {
    this.staleRows[y] = y;
  }

  setCharacterHandler(handler) {
    this.characterHandler = handler;
  }

  resetCharacterHandler() {
    this.characterHandler = DefaultHandler;
  }

  appendNewRow() {
    const rowElement = document.createElement('div');
    this.rowContainer.appendChild(rowElement);
    this.rowElements.push(rowElement);
    this.characterMatrix.push([]);
  }

  refresh() {
    Object.keys(this.staleRows).forEach((key) => {
      this.refreshRow(this.staleRows[key]);
    });

    this.staleRows = {};
  }

  refreshRow(y) {
    const row = this.rowElements[y];
    row.innerHTML = '';

    if (this.characterMatrix[y].length === 0) {
      return;
    }

    let attributes = this.characterMatrix[y][0].attributes;
    let element = Attributes.createElement(attributes);

    this.characterMatrix[y].forEach((character) => {
      if (!Attributes.equal(attributes, character.attributes)) {
        row.appendChild(element);
        attributes = character.attributes;
        element = Attributes.createElement(attributes);
      }

      element.appendChild(document.createTextNode(character.value));
    });

    row.appendChild(element);
  }

}
