import { Attributes } from './attributes';

export class Character {

  constructor(value = ' ', attributes = Attributes.default()) {
    this.attributes = Attributes.copy(attributes);
    this.value = Character.encoding[value] || value;

    if (this.value < ' ') {
      this.value = Character.encoding[' '] || ' ';
    }
  }

}

Character.encoding = {
  ' ': '\u00a0',
  '&': '\u0026',
  '<': '\u003c',
  '>': '\u003e',
};
