export class Attributes {

  static default() {
    return {};
  }

  static copy(attributes) {
    const newAttributes = {};
    Object.keys(attributes).forEach((key) => {
      newAttributes[key] = attributes[key];
    });
    return newAttributes;
  }

  static equal(a, b) {
    let equality = true;

    for (let i = 0; i < this.propertyNames.length; i++) {
      const propertyName = this.propertyNames[i];
      if (a[propertyName] !== b[propertyName]) {
        equality = false;
        break;
      }
    }

    return equality;
  }

  static createElement(attributes) {
    const element = document.createElement('span');
    const className = this.classList(attributes).join(' ');
    if (className) {
      element.className = className;
    }
    return element;
  }

  static colorClasses(attributes) {
    const classes = [];

    if (attributes.color >= 0) {
      classes.push(`xterm-color-${attributes.color}`);
    }

    if (attributes.background >= 0) {
      classes.push(`xterm-bg-color-${attributes.background}`);
    }

    return classes;
  }

  static classList(attributes) {
    const classes = this.colorClasses(attributes);

    Object.keys(this.classNames).forEach((key) => {
      if (attributes[key]) {
        classes.push(this.classNames[key]);
      }
    });

    return classes;
  }

}

Attributes.propertyNames = [
  'color',
  'background',
  'bold',
  'italic',
  'underline',
  'conceal',
  'cross',
  'colorExtended',
  'backgroundExtended',
];

Attributes.classNames = {
  bold: 'xterm-bold',
  italic: 'xterm-italic',
  underline: 'xterm-underline',
  conceal: 'xterm-conceal',
  cross: 'xterm-cross',
};
