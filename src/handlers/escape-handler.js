import { BaseHandler } from './base-handler';

export class EscapeHandler extends BaseHandler {

  static default() {
    this.resetCharacterHandler();
  }

}

EscapeHandler.handlers = {};
