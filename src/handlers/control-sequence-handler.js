import { BaseHandler } from './base-handler';

export class ControlSequenceHandler extends BaseHandler {

  static default() {
    this.resetCharacterHandler();
  }

  static pushParameter() {
    this.parameters.push(this.currentParameter);
    this.currentParameter = 0;
  }

}

ControlSequenceHandler.handlers = {};
