import { BaseHandler } from './base-handler';

import { Character } from '../character';

export class DefaultHandler extends BaseHandler {

  static default(character) {
    while (this.rowElements.length <= this.cursor.y) {
      this.appendNewRow();
    }

    while (this.characterMatrix[this.cursor.y].length < this.cursor.x) {
      this.characterMatrix[this.cursor.y].push(new Character());
    }

    this.characterMatrix[this.cursor.y][this.cursor.x] = new Character(
      character,
      this.currentAttributes
    );

    this.cursor.x++;
    this.markRowStale(this.cursor.y);
  }

}

DefaultHandler.handlers = {};
