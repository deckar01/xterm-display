import { BaseHandler } from './base-handler';

export class ExtendedColorHandler extends BaseHandler {

  static default() {
    this.ignoreCounter = 1;
  }

}

ExtendedColorHandler.handlers = [];
