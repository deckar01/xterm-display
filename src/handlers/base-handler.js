export class BaseHandler {

  static register(keys, handler) {
    keys.forEach((key) => {
      this.handlers[key] = handler;
    });
  }

  static getHandler(key) {
    return this.handlers[key] || this.default;
  }

  static default() {}

}

BaseHandler.handlers = undefined;
