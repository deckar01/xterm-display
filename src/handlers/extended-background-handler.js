import { BaseHandler } from './base-handler';

export class ExtendedBackgroundHandler extends BaseHandler {

  static default() {
    this.ignoreCounter = 1;
  }

}

ExtendedBackgroundHandler.handlers = [];
