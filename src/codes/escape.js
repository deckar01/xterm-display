import { DefaultHandler } from '../handlers/default-handler';
import { EscapeHandler } from '../handlers/escape-handler';
import { ControlSequenceHandler } from '../handlers/control-sequence-handler';

DefaultHandler.register(['\x1b'], function escape() {
  this.setCharacterHandler(EscapeHandler);
});

EscapeHandler.register(['['], function startControlSequence() {
  this.parameters = [];
  this.currentParameter = 0;
  this.setCharacterHandler(ControlSequenceHandler);
});
