import { ControlSequenceHandler } from '../handlers/control-sequence-handler';
import { StyleHandler } from '../handlers/style-handler';
import { ExtendedColorHandler } from '../handlers/extended-color-handler';
import { ExtendedBackgroundHandler } from '../handlers/extended-background-handler';

import { Attributes } from '../attributes';

ControlSequenceHandler.register(['m'], function setStyle() {
  this.resetCharacterHandler();
  ControlSequenceHandler.pushParameter.call(this);

  this.ignoreCounter = 0;
  this.parameters.forEach((parameter, i) => {
    if (this.ignoreCounter > 0) {
      this.ignoreCounter--;
      return;
    }

    const handler = StyleHandler.getHandler(parameter);
    handler.call(this, parameter, i);
  });
});

StyleHandler.register([0], function defaultStyle() {
  this.currentAttributes = Attributes.default();
});

StyleHandler.register([1], function boldStyle() {
  this.currentAttributes.bold = true;
});

StyleHandler.register([3], function italicStyle() {
  this.currentAttributes.italic = true;
});

StyleHandler.register([4], function underlineStyle() {
  this.currentAttributes.underline = true;
});

StyleHandler.register([8], function concealStyle() {
  this.currentAttributes.conceal = true;
});

StyleHandler.register([9], function crossStyle() {
  this.currentAttributes.cross = true;
});

StyleHandler.register([30, 31, 32, 33, 34, 35, 36, 37], function textColor(parameter) {
  delete this.currentAttributes.colorExtended;
  this.currentAttributes.color = parameter - 30;
});

StyleHandler.register([38], function extendedTextColor(parameter, i) {
  const handler = ExtendedColorHandler.getHandler(this.parameters[i + 1]);
  handler.call(this, parameter, i);
});

ExtendedColorHandler.register([5], function extendedTextColor256(parameter, i) {
  this.currentAttributes.colorExtended = true;
  this.currentAttributes.color = this.parameters[i + 2];
  this.ignoreCounter = 2;
});

StyleHandler.register([39], function defaultTextColor() {
  delete this.currentAttributes.colorExtended;
  delete this.currentAttributes.color;
});

StyleHandler.register([40, 41, 42, 43, 44, 45, 46, 47], function backgroundColor(parameter) {
  delete this.currentAttributes.backgroundExtended;
  this.currentAttributes.background = parameter - 40;
});

StyleHandler.register([48], function extendedBackgroundColor(parameter, i) {
  const handler = ExtendedBackgroundHandler.getHandler(this.parameters[i + 1]);
  handler.call(this, parameter, i);
});

ExtendedBackgroundHandler.register([5], function extendedBackgroundColor256(parameter, i) {
  this.currentAttributes.backgroundExtended = true;
  this.currentAttributes.background = this.parameters[i + 2];
  this.ignoreCounter = 2;
});

StyleHandler.register([49], function defaultBackgroundColor() {
  delete this.currentAttributes.backgroundExtended;
  delete this.currentAttributes.background;
});
