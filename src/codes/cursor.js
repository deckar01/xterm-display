import { DefaultHandler } from '../handlers/default-handler';
import { ControlSequenceHandler } from '../handlers/control-sequence-handler';

DefaultHandler.register(['\n', '\x0b', '\x0c'], function newLine() {
  this.cursor.y++;
  this.cursor.x = 0;
});

DefaultHandler.register(['\r'], function carriageReturn() {
  this.cursor.x = 0;
});

DefaultHandler.register(['\x08'], function moveCursorLeftOne() {
  this.cursor.x = Math.max(0, this.cursor.x - 1);
});

ControlSequenceHandler.register(['A'], function moveCursorUp() {
  this.resetCharacterHandler();
  ControlSequenceHandler.pushParameter.call(this);

  const offset = (this.parameters[0] || 1);
  this.cursor.y = Math.max(0, this.cursor.y - offset);
});

ControlSequenceHandler.register(['B'], function moveCursorDown() {
  this.resetCharacterHandler();
  ControlSequenceHandler.pushParameter.call(this);

  this.cursor.y += (this.parameters[0] || 1);
});

ControlSequenceHandler.register(['C'], function moveCursorRight() {
  this.resetCharacterHandler();
  ControlSequenceHandler.pushParameter.call(this);

  this.cursor.x += (this.parameters[0] || 1);
});

ControlSequenceHandler.register(['D'], function moveCursorLeft() {
  this.resetCharacterHandler();
  ControlSequenceHandler.pushParameter.call(this);

  const offset = (this.parameters[0] || 1);
  this.cursor.x = Math.max(0, this.cursor.x - offset);
});

ControlSequenceHandler.register(['E'], function moveCursorRowUp() {
  this.resetCharacterHandler();
  ControlSequenceHandler.pushParameter.call(this);

  this.cursor.y += (this.parameters[0] || 1);
  this.cursor.x = 0;
});

ControlSequenceHandler.register(['F'], function moveCursorRowUp() {
  this.resetCharacterHandler();
  ControlSequenceHandler.pushParameter.call(this);

  const offset = (this.parameters[0] || 1);
  this.cursor.y = Math.max(0, this.cursor.y - offset);
  this.cursor.x = 0;
});

ControlSequenceHandler.register(['G'], function setCursorColumn() {
  this.resetCharacterHandler();
  ControlSequenceHandler.pushParameter.call(this);

  this.cursor.x = Math.max(0, (this.parameters[0] || 1) - 1);
});

ControlSequenceHandler.register(['H', 'f'], function setCursorPosition() {
  this.resetCharacterHandler();
  ControlSequenceHandler.pushParameter.call(this);

  this.cursor.y = Math.max(0, (this.parameters[0] || 1) - 1);
  this.cursor.x = Math.max(0, (this.parameters[1] || 1) - 1);
});
