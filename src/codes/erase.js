import { ControlSequenceHandler } from '../handlers/control-sequence-handler';
import { DisplayClearHandler } from '../handlers/display-clear-handler';
import { LineClearHandler } from '../handlers/line-clear-handler';

import { Character } from '../character';

ControlSequenceHandler.register(['J'], function eraseInDisplay() {
  this.resetCharacterHandler();
  ControlSequenceHandler.pushParameter.call(this);

  const parameter = this.parameters[0] || 0;
  const handler = DisplayClearHandler.getHandler(parameter);
  handler.call(this);
});

DisplayClearHandler.register([0], function eraseToDisplayEnd() {
  if (this.characterMatrix[this.cursor.y]) {
    this.characterMatrix[this.cursor.y].splice(this.cursor.x);
    this.markRowStale(this.cursor.y);
  }

  for (let y = this.cursor.y + 1; y < this.characterMatrix.length; y++) {
    this.characterMatrix[y].length = 0;
    this.markRowStale(y);
  }
});

DisplayClearHandler.register([1], function eraseToDisplayBeginning() {
  if (this.characterMatrix[this.cursor.y]) {
    for (let x = 0; x <= this.cursor.x; x++) {
      this.characterMatrix[this.cursor.y][x] = new Character();
    }
    this.markRowStale(this.cursor.y);
  }

  for (let y = 0; y <= this.cursor.y - 1; y++) {
    this.characterMatrix[y].length = 0;
    this.markRowStale(y);
  }
});

DisplayClearHandler.register([2], function eraseDisplay() {
  for (let y = 0; y < this.characterMatrix.length; y++) {
    this.characterMatrix[y].length = 0;
    this.markRowStale(y);
  }
});

ControlSequenceHandler.register(['K'], function eraseInLine() {
  this.resetCharacterHandler();
  ControlSequenceHandler.pushParameter.call(this);

  const parameter = this.parameters[0] || 0;
  const handler = LineClearHandler.getHandler(parameter);
  handler.call(this);
});

LineClearHandler.register([0], function eraseToLineEnd() {
  if (this.characterMatrix[this.cursor.y]) {
    this.characterMatrix[this.cursor.y].splice(this.cursor.x);
    this.markRowStale(this.cursor.y);
  }
});

LineClearHandler.register([1], function eraseToLineBeginning() {
  if (this.characterMatrix[this.cursor.y]) {
    for (let x = 0; x <= this.cursor.x; x++) {
      this.characterMatrix[this.cursor.y][x] = new Character();
    }
    this.markRowStale(this.cursor.y);
  }
});

LineClearHandler.register([2], function eraseLine() {
  if (this.characterMatrix[this.cursor.y]) {
    this.characterMatrix[this.cursor.y].splice(0);
    this.markRowStale(this.cursor.y);
  }
});
