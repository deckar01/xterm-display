import { ControlSequenceHandler } from '../handlers/control-sequence-handler';

ControlSequenceHandler.register('0123456789'.split(''), function parseParameter(character) {
  this.currentParameter *= 10;
  this.currentParameter += character.charCodeAt(0) - 48;
});

ControlSequenceHandler.register([';'], ControlSequenceHandler.pushParameter);
