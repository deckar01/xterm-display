# xterm-display

[![build status](https://gitlab.com/deckar01/xterm-display/badges/master/build.svg)](https://gitlab.com/deckar01/xterm-display/commits/master)
[![coverage report](https://gitlab.com/deckar01/xterm-display/badges/master/coverage.svg)](https://gitlab.com/deckar01/xterm-display/commits/master)

## About

This library is port of [xterm.js](https://github.com/sourcelair/xterm.js) that
focuses on display emulation and supporting customization.

## Usage

### Install

```sh
bower install --save xterm-display
```

### Include

```html
<head>
  <link rel="stylesheet" href="bower_components/xterm-display/dist/xterm.css">
</head>

<body>
  <div id="terminal" class="terminal xterm">
    <div id="terminal-rows" class="xterm-rows"></div>
  </div>

  <script src="bower_components/xterm-display/dist/xterm-display.min.js"></script>
</body>
```

### Use

**Basic**

```js
var terminalContainer = document.getElementById('terminal');
var rowContainer = document.getElementById('terminal-rows');

var terminal = new xterm.Terminal(terminalContainer, rowContainer);

terminal.write('hello world');
```

**Async**

Writing large amounts of data to the DOM synchronously can cause browsers to hang.
The `AsyncTerminal` class can be used to write the data asynchronously in smaller chunks.
The `chunkSize` and `delay` parameters to `AsyncTerminal.write()` can be used to
tweak the performance and timing of the write operations. The `tick` callback
can be used to monitor when each chunk is written, and the `done` callback can
be used to perform an action after all of the data has been written.

```js
var terminal = new xterm.AsyncTerminal(terminalContainer, rowContainer);

terminal.write('hello world\n');

terminal.write('hello mars!', {
  chunkSize: 2,
  delay: 20,
  tick: function(rowCount) { console.log(rowCount); },
  done: function(rowCount) { console.log('Done!'); }
});
```

## Customization

This library was designed to be fully modular so that any behavior can be customized, extended, or added as needed without needing to fork or modify the core logic.

### Customize Class Names

```js
xterm.Attributes.classNames.bold = 'custom-bold';
```

### Add a custom style code

```js
xterm.Attributes.propertyNames.push('shiny');
xterm.Attributes.classNames.shiny = 'xterm-shiny';

StyleHandler.register([777], function shinyStyle() {
  this.currentAttributes.shiny = true;
});

terminal.write('\x1b[777mSo shiny');
// <span class="xterm-shiny">So shiny</span>
```

See [src/codes/](https://gitlab.com/deckar01/xterm-display/tree/master/src/codes/) for more examples of registering codes and commands.

### Custom Character Encoding

```js
Character.encoding['\x07'] = '\u2407';

var character = new Character('\x07');
// character.value == '\u2407'
```

### Custom Core Logic

More complicated functionality my require overriding the core logic. You should prefer adding new class methods over replacing existing class methods when possible.

**Extend**

Example of adding a new class method:

```js
xterm.Terminal.prototype.writeln = function(text) {
  this.write(text + '\n');
}
```

**Override**

Example of overriding an existing class method:

```js
xterm.Attributes.appendNewRow = function(text) {
  var rowElement = document.createElement('div');
  var rowDataElement = document.createElement('div');
  var lineNumber = document.createElement('span');
  lineNumber.innerHTML = this.rowElements.length + 1;
  this.rowElement.appendChild(rowDataElement);
  this.rowElement.appendChild(rowDataElement);
  this.rowContainer.appendChild(rowElement);
  this.rowElements.push(rowDataElement);
  this.characterMatrix.push([]);
}
```
