var webpack = require('webpack');
var baseConfig = require('./webpack.config.base.js');

var config = Object.create(baseConfig);
config.output.libraryTarget = 'var';

module.exports = config;
